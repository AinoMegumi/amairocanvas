# あまいろキャンバス　～白いキャンバスの誓い～

## 注意事項

* この物語は、2017年6月26日現在開発中の恋愛シミュレーションゲーム「あまいろパステル　～パステルカラーの恋～」のモブキャラの恋愛を書いた物語です。そのため、ゲームのキャラクターも登場しますが、ゲーム本編とは関係ありません。また、ゲームと同様この作品もフィクションであり、実在の人物・団体・行事等は一切関係ありません。
* 本作品の二次創作等を行いたい場合は、行うことの詳細を通知し、許可を得てください。ただし、あまいろパステルに関わる部分については今のところは一切禁止します。
* このリポジトリ及びHTML公開用リポジトリのフォークを禁止します。もしフォークした場合でも、それに対するプルリクエストは一切受け付けません。
* 本リポジトリのIssueへの書き込みはしないでください。もし作者に対して言いたいことがありましたら、[Twitter](https://twitter.com/kamioda_ampsprg)にリプしてください。

## 作者情報

* 作者名：あいめぐ
* Twitter：[@kamioda\_ampsprg](https://twitter.com/kamioda_ampsprg)

## 作品のネタ元

* [天色＊アイルノーツ\(ゆずソフト\)](http://www.yuzu-soft.com/ja/amairo.html)
* [オトメ＊ドメイン\(ぱれっとクオリア\)](http://qualia.clearrave.co.jp/)
* [桜舞う乙女のロンド\(ensemble♪\)](http://www.ensemble-game.com/07.sakuoto/)
* [乙女が結ぶ月夜の煌めき\(ensemble♪\)](http://www.ensemble-game.com/21.ototsuki/)
* [お嬢様と秘密の乙女\(MOONSTONE Honey\)](http://www.moon-stone.jp/product/ms20/)



